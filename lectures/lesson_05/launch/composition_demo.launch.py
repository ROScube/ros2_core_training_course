# Copyright 2019 ADLINK Technology Ltd. Advanced Robotic Platform Group
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import launch

from launch_ros.actions import ComposableNodeContainer
from launch_ros.actions import Node
from launch_ros.descriptions import ComposableNode



def generate_launch_description():
    container = ComposableNodeContainer(
            node_name="Lesson05Container",
            node_namespace="",
            package="rclcpp_components",
            node_executable="component_container",
            composable_node_descriptions=[
                ComposableNode(package = 'lesson_05', node_plugin = 'lesson_05::TalkerComponent', node_name = 'talker'),
                ComposableNode(package = 'lesson_05', node_plugin = 'lesson_05::TalkerComponent', node_name = 'talker_02'),
                ComposableNode(package = 'lesson_05', node_plugin = 'lesson_05::ListenerComponent', node_name = 'listener'),
                ComposableNode(package = 'lesson_05', node_plugin = 'lesson_05::ListenerComponent', node_name = 'listener_02'),
                ],
            output='screen'
            )
    container_02 = ComposableNodeContainer(
            node_name="Lesson05Container02",
            node_namespace="",
            package="rclcpp_components",
            node_executable="component_container",
            composable_node_descriptions=[
                ComposableNode(package = 'lesson_05', node_plugin = 'lesson_05::TalkerComponent', node_name = 'talker_03'),
                ComposableNode(package = 'lesson_05', node_plugin = 'lesson_05::TalkerComponent', node_name = 'talker_10'),
                ComposableNode(package = 'lesson_05', node_plugin = 'lesson_05::ListenerComponent', node_name = 'listener_09'),
                ComposableNode(package = 'lesson_05', node_plugin = 'lesson_05::ListenerComponent', node_name = 'listener_11'),
                ],
            output='screen'
            )
    return launch.LaunchDescription([container, container_02])
