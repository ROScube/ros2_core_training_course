import launch
from launch_ros.actions import Node


def generate_launch_description():
    nodes = list()
    for i in range(2):
        _talker_node = Node(
                package='lesson_02',
                node_executable='talker_node',
                node_name='talker_node_{}'.format(i),
                output='screen')
        _listener_node = Node(
                package='lesson_02',
                node_executable='listener_node',
                node_name='listener_node_{}'.format(i),
                output='screen')
        nodes.append(_talker_node)
        nodes.append(_listener_node)
    return launch.LaunchDescription(nodes)
