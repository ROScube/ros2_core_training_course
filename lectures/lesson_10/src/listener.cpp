// Copyright 2019 ADLINK Technology Ltd. Advanced Robotic Platform Group
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <chrono>

#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/string.hpp>

using namespace std::chrono_literals;

class ListenerNode : public rclcpp::Node {
  public:
    ListenerNode(const rclcpp::NodeOptions & options = rclcpp::NodeOptions()) :
      Node("Listener", options)
  {
      auto sub_cb =
        [this](const std_msgs::msg::String::SharedPtr msg) {
          RCLCPP_INFO(this->get_logger(), "Msg received: %s",
              msg->data.c_str());
        };
      rclcpp::QoS qos = rclcpp::QoS(10).reliable().transient_local().lifespan(5s);
      this->_sub = this->create_subscription<std_msgs::msg::String>
        ("lesson_10", qos, sub_cb);
    };
  private:
    rclcpp::Subscription<std_msgs::msg::String>::SharedPtr _sub;
};

int main (int argc, char * argv[]) {
  rclcpp::init(argc, argv);
  auto node = std::make_shared<ListenerNode>();
  rclcpp::spin(node);
  rclcpp::shutdown();
  return 0;
}
