// Copyright 2019 ADLINK Technology Ltd. Advanced Robotic Platform Group
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <chrono>

#include <rclcpp/rclcpp.hpp>
#include <rclcpp/publisher.hpp>
#include <rclcpp_lifecycle/lifecycle_node.hpp>
#include <rclcpp_lifecycle/lifecycle_publisher.hpp>
#include <lifecycle_msgs/msg/transition.hpp>
#include <std_msgs/msg/string.hpp>

using namespace std::chrono_literals;

class LifecycleTalkerNode : public rclcpp_lifecycle::LifecycleNode {
  public:
    explicit LifecycleTalkerNode(const rclcpp::NodeOptions & options = rclcpp::NodeOptions()) :
      rclcpp_lifecycle::LifecycleNode("LifecycleTalkerNode", options) {}

    ~LifecycleTalkerNode(){}

    rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
    on_configure(const rclcpp_lifecycle::State &)
    {
      this->_pub = this->create_publisher<std_msgs::msg::String>("lesson_06", 10);
      auto timer_cb =
        [this]()-> void {
          std_msgs::msg::String::UniquePtr msg_ =
            std::make_unique<std_msgs::msg::String>();
          msg_->data = "Hello World, counter " + std::to_string(_counter ++);
          if (!this->_pub->is_activated()) {
            RCLCPP_INFO(this->get_logger(), "lifecycle node os inactivate, message will not published");
          } else {
            RCLCPP_INFO(this->get_logger(), "Publishing message: %s", msg_->data.c_str());
          }
          this->_pub->publish(std::move(msg_));
        };
      this->_timer = this->create_wall_timer(1s, timer_cb);
      RCLCPP_INFO(this->get_logger(), "on_configure() is called");
      return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
    }

    rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
    on_activate(const rclcpp_lifecycle::State &)
    {
      this->_pub->on_activate();
      RCLCPP_INFO(this->get_logger(), "on_activate() is called");
      // Assume we are doing something
      std::this_thread::sleep_for(2s);
      return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
    }

    rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
    on_deactivate(const rclcpp_lifecycle::State &)
    {
      this->_pub->on_deactivate();
      RCLCPP_INFO(this->get_logger(), "on_deactivate() is called");
      return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
    }

    rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
    on_cleanup(const rclcpp_lifecycle::State &)
    {
      this->_timer.reset();
      this->_pub.reset();
      RCLCPP_INFO(this->get_logger(), "on_cleanup() is called");
      return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
    }

    rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
    on_shutdown(const rclcpp_lifecycle::State & state)
    {
      RCLCPP_INFO(this->get_logger(), "on_shutdown() is called from state %s", state.label().c_str());
      return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
    }

  private:
    uint32_t _counter = 0;
    rclcpp_lifecycle::LifecyclePublisher<std_msgs::msg::String>::SharedPtr _pub;
    rclcpp::TimerBase::SharedPtr _timer;
};


int main (int argc, char *argv[]) {
  setvbuf(stdout, NULL, _IONBF, BUFSIZ);
  rclcpp::init(argc, argv);
  auto node = std::make_shared<LifecycleTalkerNode>();
  rclcpp::spin(node->get_node_base_interface());
  rclcpp::shutdown();
  return 0;
}
