// Copyright 2019 ADLINK Technology Ltd. Advanced Robotic Platform Group
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <cinttypes>
#include <cstdio>
#include <rclcpp/rclcpp.hpp>
#include <lesson_03/srv/multiply_two_ints.hpp>

class SimpleServerNode : public rclcpp::Node {
  public:
    SimpleServerNode(const rclcpp::NodeOptions & options = rclcpp::NodeOptions())
      : Node("multiply_two_ints_server", options)
    {
      auto handle_multiply_two_ints =
        [this](const std::shared_ptr<rmw_request_id_t> request_header,
               const std::shared_ptr<lesson_03::srv::MultiplyTwoInts::Request> request,
               std::shared_ptr<lesson_03::srv::MultiplyTwoInts::Response> response) -> void
        {
          (void) request_header;
          RCLCPP_INFO(this->get_logger(), "Incoming request: a=%" PRId64 " b=%" PRId64,
              request->a, request->b);
          response->multiply = request->a * request->b;
        };
      this->srv_ = this->create_service<lesson_03::srv::MultiplyTwoInts>("multiply_two_ints",
          handle_multiply_two_ints);
    }
  private:
    rclcpp::Service<lesson_03::srv::MultiplyTwoInts>::SharedPtr srv_;
};

int main (int argc, char * argv[]) {
  rclcpp::init(argc, argv);
  auto node = std::make_shared<SimpleServerNode>();
  rclcpp::spin(node);
  rclcpp::shutdown();
  return 0;
}
