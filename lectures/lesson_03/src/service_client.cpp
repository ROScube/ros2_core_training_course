// Copyright 2019 ADLINK Technology Ltd. Advanced Robotic Platform Group
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include <cinttypes>
#include <cstdio>
#include <chrono>
#include <rclcpp/rclcpp.hpp>
#include <lesson_03/srv/multiply_two_ints.hpp>

using namespace std::chrono_literals;

class SimpleClientNode : public rclcpp::Node {
  public:
    SimpleClientNode(const rclcpp::NodeOptions & options = rclcpp::NodeOptions())
      : Node("multiply_two_ints_client", options)
    {
      this->client_ = this->create_client<lesson_03::srv::MultiplyTwoInts>("multiply_two_ints");
    }
    rclcpp::Client<lesson_03::srv::MultiplyTwoInts>::SharedPtr client_;
};

int main (int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  auto node = std::make_shared<SimpleClientNode>();
  while (!node->client_->wait_for_service(1s)) {
    if (!rclcpp::ok()) {
      RCLCPP_INFO(node->get_logger(), "Interrupt while waiting the service. Exiting");
      return 0;
    }
    RCLCPP_INFO(node->get_logger(), "service not available, waiting again");
  }
  auto response_received_callback =
    [node](rclcpp::Client<lesson_03::srv::MultiplyTwoInts>::SharedFuture future) -> void
    {
      auto result = future.get();
      RCLCPP_INFO(node->get_logger(), "Result of multiply_two_ints is %" PRId64, result->multiply);
      rclcpp::shutdown();
    };
  auto request = std::make_shared<lesson_03::srv::MultiplyTwoInts::Request>();
  request->a = 10;
  request->b = 200;
  auto future_result = node->client_->async_send_request(request, response_received_callback);
  /* If you want to sync wait, use rclcpp::spin_until_future_complete
  if (rclcpp::spin_until_future_complete(node, future_result)
      == rclcpp::executor::FutureReturnCode::SUCCESS) {
    RCLCPP_INFO(node->get_logger(), "sync wait for service result finished");
  }
  */
  rclcpp::spin(node);
  rclcpp::shutdown();
  return 0;
}
