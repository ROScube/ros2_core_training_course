// Copyright 2019 ADLINK Technology Ltd. Advanced Robotic Platform Group
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <chrono>

#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/string.hpp>

using namespace std::chrono_literals;

class TalkerNode : public rclcpp::Node {
  public:
    TalkerNode(const rclcpp::NodeOptions & options = rclcpp::NodeOptions()) :
      Node("TalkerNode", options) {

      rclcpp::QoS qos = rclcpp::QoS(10).deadline(100ms).best_effort();

      rclcpp::PublisherOptions pub_options;
      pub_options.event_callbacks.deadline_callback =
        [this](rclcpp::QOSDeadlineOfferedInfo & event)->void {
          RCLCPP_INFO(this->get_logger(),
              "Offered deadline miss match, total %d delta %d",
              event.total_count, event.total_count_change);
        };

      this->_pub = this->create_publisher<std_msgs::msg::String>
        ("lesson_02", qos, pub_options);

      auto timer_cb =
        [this]()-> void {
          std_msgs::msg::String::UniquePtr msg_ =
            std::make_unique<std_msgs::msg::String>();
          msg_->data = "Hello World, counter " + std::to_string(_counter ++);
          RCLCPP_INFO(this->get_logger(), "Publishing message: %s",
              msg_->data.c_str());
          this->_pub->publish(std::move(msg_));
        };
      this->_timer = this->create_wall_timer(1s, timer_cb);
    };
    ~TalkerNode() {
    }
  private:
    uint32_t _counter = 0;
    rclcpp::Publisher<std_msgs::msg::String>::SharedPtr _pub;
    rclcpp::TimerBase::SharedPtr _timer;
};


int main (int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  auto node = std::make_shared<TalkerNode>();
  rclcpp::spin(node);
  rclcpp::shutdown();
  return 0;
}
